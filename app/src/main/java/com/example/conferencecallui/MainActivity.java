package com.example.conferencecallui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity {
    FrameLayout mConferenceLayout, mNormal_Call_Layout;
    Button mSwitchButton;
    Chronometer mChronometer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mConferenceLayout = findViewById(R.id.conference_layout);
        mNormal_Call_Layout = findViewById(R.id.normal_call_layout);
        mSwitchButton = findViewById(R.id.switch_button);
        mChronometer = findViewById(R.id.timer_chronometer);
        mChronometer.start();

        mSwitchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchLayout();
            }
        });
    }

    public void switchLayout(){
        if(mConferenceLayout.getVisibility() == View.GONE){
            mConferenceLayout.setVisibility(View.VISIBLE);
            mNormal_Call_Layout.setVisibility(View.GONE);
        }
      else if(mNormal_Call_Layout.getVisibility() == View.GONE){
            mNormal_Call_Layout.setVisibility(View.VISIBLE);
            mConferenceLayout.setVisibility(View.GONE);


        }

    }
}